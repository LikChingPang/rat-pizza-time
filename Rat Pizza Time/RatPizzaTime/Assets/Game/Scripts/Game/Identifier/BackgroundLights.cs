using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Background Light item identifier
/// </summary>

[RequireComponent(typeof(Light))]
public class BackgroundLights : MonoBehaviour
{
    public void SwitchColor(Color color)
    {
        gameObject.GetComponent<Light>().color = color;
    }
}
