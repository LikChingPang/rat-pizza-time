using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Wire item Identifier
/// </summary>

public class Wire : MonoBehaviour
{
    public bool isBroken = false;
    
    public GameObject brokenState, normalState;

    void Start()
    {
        // If the wire is not a cheeseable wire
        if (gameObject.GetComponent<Cheesed>() == null)
        {
            // If the wire is broken
            if (isBroken == true)
            {
                // Show a broken wire
                brokenState.SetActive(true);
                normalState.SetActive(false);
            }
            // If the wire is not broken
            else
            {
                // Show a complete wire
                brokenState.SetActive(false);
                normalState.SetActive(true);
            }
        }
        // If the wire is a cheeseable wire
        else
        {
            // Get the cheesed component from the wire
            Cheesed cheesedWire = gameObject.GetComponent<Cheesed>();

            // If the wire is already cheesed
            if (cheesedWire.cheesed == true)
            {
                // If the wire is broken
                if (isBroken == true)
                {
                    // Show a broken cheesed wire
                    brokenState.SetActive(false);
                    normalState.SetActive(false);
                    cheesedWire.cheesedBrokenState.SetActive(true);
                    cheesedWire.cheesedNormalState.SetActive(false);
                }
                // If the wire is not broken
                else
                {
                    // Show a complete cheesed wire
                    brokenState.SetActive(false);
                    normalState.SetActive(false);
                    cheesedWire.cheesedBrokenState.SetActive(false);
                    cheesedWire.cheesedNormalState.SetActive(true);
                }
            }
            // If the wire is not cheesed yet
            else
            {
                // If the wire is broken
                if (isBroken == true)
                {
                    // Show a broken wire
                    brokenState.SetActive(true);
                    normalState.SetActive(false);
                    cheesedWire.cheesedBrokenState.SetActive(false);
                    cheesedWire.cheesedNormalState.SetActive(false);
                }
                // If the wire is not broken
                else
                {
                    // Show a complete wire
                    brokenState.SetActive(false);
                    normalState.SetActive(true);
                    cheesedWire.cheesedBrokenState.SetActive(false);
                    cheesedWire.cheesedNormalState.SetActive(false);
                }
            }
        }
    }

    public void PlayAnimation()
    {
        GetComponent<Animation>().Play();
    }
}
