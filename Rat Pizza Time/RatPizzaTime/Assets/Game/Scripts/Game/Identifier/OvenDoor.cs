using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Oven door item identifier
/// </summary>

public class OvenDoor : MonoBehaviour
{
    [SerializeField] AudioSource openDoorSound, closeDoorSound;

    public void OpenDoor()
    {
        // Open the door
        transform.parent.eulerAngles = new Vector3(0, 0, 90);

        // Play the open door sound
        closeDoorSound.Play();
    }

    public void CloseDoor()
    {
        // Close the door
        transform.parent.eulerAngles = new Vector3(0, 0, 0);

        // Play the close door sound
        openDoorSound.Play();
    }
}
