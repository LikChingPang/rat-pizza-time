using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Reset button item identifier
/// </summary>

[RequireComponent(typeof(Interactable), typeof(AudioSource))]
public class ResetButton : MonoBehaviour
{

}
