using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Cheese item identifier
/// </summary>

[RequireComponent(typeof(Rigidbody), typeof(BoxCollider), typeof(Interactable))]
[RequireComponent(typeof(AudioSource))]
public class Cheese : MonoBehaviour
{
    [HideInInspector] public bool thrown = false;

    void OnCollisionEnter(Collision collision)
    {
        // If it is a wall that the cheese can stick on
        if (collision.gameObject.GetComponent<Oven>() != null)
        {
            // Don't allow the cheese to move anymore
            gameObject.GetComponent<Rigidbody>().isKinematic = true;

            // If the cheese was thrown
            if (thrown == true)
            {
                // Play the sticky cheese sound
                gameObject.GetComponent<AudioSource>().Play();

                // Reset thrown
                thrown = false;
            }
        }
    }
}
