using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// This script is responsible for detecting if the player is on the ground
/// </summary>

public class GroundDetecter : MonoBehaviour
{
    [HideInInspector] public bool isOnGround = false;

    void OnTriggerStay(Collider other)
    {
        // If the collider is not hitting the rat or the rail
        if (other.GetComponent<RatMovement>() == null && other.GetComponent<Rail>() == null)
        {
            // The player is on the ground
            isOnGround = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        // The player is not on the ground
        isOnGround = false;
    }
}
