using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// This script is reponsible for the behaviour for the Circuit Board 
/// </summary>

public class CircuitBoard : MonoBehaviour
{
    [SerializeField] Transform tooltipUI, puzzleDoor;
    [SerializeField] GameObject resetButton;
    [SerializeField] bool randomOrder;
    [SerializeField] List<LightWire> puzzleOrder = new List<LightWire>();
    
    List<LightWire> inputOrder = new List<LightWire>();
    List<int> intOrder = new List<int>();

    int currentInputOrderNumber = 0;

    [HideInInspector] public bool isComplete = false;

    void Start()
    {
        // Initialize
        isComplete = false;
        currentInputOrderNumber = 0;

        if (randomOrder == true)
        {
            // File the order board
            for (int i = 0; i < puzzleOrder.Count; i++)
            {
                // With the order number
                intOrder.Add(i);
            }

            // Shuffle the order list
            Shuffle(intOrder.Count, intOrder);

            for (int i = 0; i < puzzleOrder.Count; i++)
            {
                puzzleOrder[intOrder[i]] = transform.GetChild(i).GetComponent<LightWire>();
            }
        }

        for (int i = 0; i < puzzleOrder.Count; i++)
        {
            inputOrder.Add(null);
        }
    }

    void Shuffle(int listChildCount, List<int> order)
    {
        int n = listChildCount;
        // If n == 0, it's the first card of the deck
        if (n == 0)
        {
            // Do nothing
        }
        else
        {
            // Shuffling from the bottom of the deck to the first, deck size amount of times
            Shuffle(--n, order);
            int toBeShuffle = Random.Range(0, listChildCount - 1);
            SwapCard(toBeShuffle, n, intOrder);
        }
    }

    // change this
    void SwapCard(int toBeShuffle, int currentCard, List<int> intOrderList)
    {
        // Card swap
        int cardShuffler = intOrderList[toBeShuffle];
        intOrderList[toBeShuffle] = intOrderList[currentCard];
        intOrderList[currentCard] = cardShuffler;
    }

    public void CheckStatus()
    {
        // For each element in inputOrder
        for (int j = 0; j < inputOrder.Count; j++)
        {
            // Compare each element in both inputOder and puzzleOrder
            if (inputOrder[j] != puzzleOrder[j])
            {
                // Display the wrong light to tell the player that the order is wrong
                inputOrder[j].lightBulb.GetComponent<Renderer>().material = inputOrder[j].gameObject.GetComponent<LightWire>().wrongLight;
            }
            else
            {
                // Display the correct light to tell the player that the order is right
                inputOrder[j].lightBulb.GetComponent<Renderer>().material = inputOrder[j].gameObject.GetComponent<LightWire>().correctLight;
            }
        }
    }

    public void InputOrder(LightWire lightwire)
    {
        // If the list elements are not filled up
        if (currentInputOrderNumber < inputOrder.Count)
        {
            // Disable the cheesed component so that it cannot be rebroken
            lightwire.cheeseableWire.GetComponent<Cheesed>().cheesed = false;

            // For each tooltip
            for (int i = 0; i < tooltipUI.childCount; i++)
            {
                // Disable the tooltip
                tooltipUI.GetChild(i).gameObject.SetActive(false);
            }

            // Input the lightwire to the element
            inputOrder[currentInputOrderNumber++] = lightwire;
        }

        // If the currentInputOrderNumber equal to the input order's list size (which means the last list element filled up)
        if (currentInputOrderNumber == inputOrder.Count)
        {
            // Display result
            CheckStatus();

            // For each element in inputOrder
            for (int i = 0; i < inputOrder.Count; i++)
            {
                // If that element is not the same same element in puzzleOrder with the same order
                if (inputOrder[i] != puzzleOrder[i])
                {
                    // GTFO
                    return;
                }
            }

            // Set isComplete to true
            isComplete = true;

            // Set the resetButton to inactive
            resetButton.SetActive(false);

            // Play the open door sound
            puzzleDoor.GetComponent<AudioSource>().Play();

            // Open the puzzle door with animation
            puzzleDoor.GetComponent<Animation>().Play();
        }
    }

    public void ResetPuzzle()
    {
        // Reset currentInputOrderNumber
        currentInputOrderNumber = 0;

        // For each element in inputOrder
        for (int j = 0; j < inputOrder.Count; j++)
        {
            // Safety warp
            if (inputOrder[j] != null)
            {
                // Reset the wire to the broken cheesed state
                inputOrder[j].cheeseableWire.GetComponent<Cheesed>().cheesed = true;
                inputOrder[j].cheeseableWire.GetComponent<Wire>().isBroken = true;
                inputOrder[j].cheeseableWire.GetComponent<Cheesed>().cheesedNormalState.SetActive(false);
                inputOrder[j].cheeseableWire.GetComponent<Cheesed>().cheesedBrokenState.SetActive(true);

                // Reset the light
                inputOrder[j].lightBulb.GetComponent<Renderer>().material = inputOrder[j].gameObject.GetComponent<LightWire>().glassLight;

                // Reset the element to null
                inputOrder[j] = null;
            }
        }
    }
}
