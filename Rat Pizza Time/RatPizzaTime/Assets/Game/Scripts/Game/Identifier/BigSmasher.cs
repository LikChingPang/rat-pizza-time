using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(BoxCollider), typeof(Rigidbody))]
public class BigSmasher : MonoBehaviour
{
    [SerializeField] float boostUpForce = 1000f;

    public void BoostUp()
    {
        gameObject.GetComponent<Rigidbody>().AddForce(Vector3.up * boostUpForce);
    }
}
