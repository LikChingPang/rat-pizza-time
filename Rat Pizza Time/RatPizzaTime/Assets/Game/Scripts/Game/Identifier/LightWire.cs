using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightWire : MonoBehaviour
{
    public GameObject cheeseableWire;

    public Transform lightBulb;

    public Material glassLight, correctLight, wrongLight;
}
