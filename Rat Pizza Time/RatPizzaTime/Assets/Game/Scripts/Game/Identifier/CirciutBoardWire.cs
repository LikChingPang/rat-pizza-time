using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Circuit board wire item identifier
/// </summary>

[RequireComponent(typeof(Wire), typeof(Cheesed))]
public class CirciutBoardWire : MonoBehaviour
{
    void Awake()
    {
        gameObject.GetComponent<Wire>().isBroken = true;
        gameObject.GetComponent<Cheesed>().cheesed = true;
    }
}
