using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Fan item identifier
/// </summary>

[RequireComponent(typeof(AudioSource))]
public class Fan : MonoBehaviour
{
    [SerializeField] Transform toCutWires, notToCutWires;
    [SerializeField] GameObject blocker;

    [HideInInspector] public bool jammed = false;

    public void CheckStatus()
    {
        if (jammed == false)
        {
            for (int i = 0; i < toCutWires.childCount; i++)
            {
                // If that child in toCutWires has a wire combonent
                if (toCutWires.GetChild(i).GetComponent<Wire>() != null)
                {
                    // If one of them is not cut
                    if (toCutWires.GetChild(i).GetComponent<Wire>().isBroken == false)
                    {
                        // Get out of here
                        return;
                    }
                }
                // Else
                else
                {
                    // Get out of here
                    return;
                }
            }

            for (int i = 0; i < notToCutWires.childCount; i++)
            {
                // If that child in notToCutWires has a wire combonent
                if (notToCutWires.GetChild(i).GetComponent<Wire>() != null)
                {
                    // If one of them is cut
                    if (notToCutWires.GetChild(i).GetComponent<Wire>().isBroken == true)
                    {

                        // Get out of here
                        return;
                    }
                }
                // Else
                else
                {
                    // Get out of here
                    return;
                }
            }

            // Set jammed to true
            jammed = true;

            // Stop the fan sound
            gameObject.GetComponent<AudioSource>().Stop();

            // Unblock the fan
            blocker.SetActive(false);
        }
    }
}
