using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// This script is responsible for handling the item putdown events
/// </summary>

[RequireComponent(typeof(RatInventory), typeof(ItemPickUp))]
public class ItemPutDown : MonoBehaviour
{
    KeyCode interact;
    GameObject ratInventoryGameObject;

    [SerializeField] float throwPower = 250f;
    [SerializeField] Transform backObjects, tooltipUI;
    [SerializeField] PutDownItemLocation putDownItemLocation;

    void Start()
    {
        // Initialize
        interact = gameObject.GetComponent<ItemPickUp>().interact;
    }

    void Update()
    {
        // If the interact key is pressed
        if (Input.GetKeyDown(interact))
        {
            // Get the current ratInventory's inventoryGameObject
            ratInventoryGameObject = gameObject.GetComponent<RatInventory>().inventoryGameObject;

            // If there is an inventory GameObject
            if (ratInventoryGameObject != null)
            {
                // Play the drop item sound
                backObjects.GetComponent<AudioSource>().Play();

                // Set the rotation of the gameObject in the rat's inventory to the rotation of the rat
                ratInventoryGameObject.transform.eulerAngles = new Vector3(ratInventoryGameObject.transform.eulerAngles.x, transform.eulerAngles.y, ratInventoryGameObject.transform.eulerAngles.z);

                // Set the position of the gameObject in the rat's inventory to the position of the rat
                ratInventoryGameObject.transform.position = new Vector3(transform.position.x, transform.position.y + putDownItemLocation.GetComponent<CapsuleCollider>().height, transform.position.z);

                // Show the gameObject in rat's inventory
                ratInventoryGameObject.SetActive(true);

                // Throw that food
                // Do this because of the fbx is messed up
                // If the gameObject is a Bacon
                if (ratInventoryGameObject.GetComponent<Bacon>() != null)
                {
                    // * 1.5f because bacon is larger
                    ratInventoryGameObject.GetComponent<Rigidbody>().AddForce(transform.forward * throwPower * 1.5f);
                }
                // If it is anything else
                else
                {
                    ratInventoryGameObject.GetComponent<Rigidbody>().AddForce(transform.forward * throwPower);
                }

                // If the dropped object is a cheese
                if (ratInventoryGameObject.GetComponent<Cheese>() != null)
                {
                    // Allow it to play drop sound
                    ratInventoryGameObject.GetComponent<Cheese>().thrown = true;
                }

                // Go through every back item on the rat
                for (int i = 0; i < backObjects.childCount; i++)
                {
                    // Set them to inactive
                    backObjects.GetChild(i).gameObject.SetActive(false);
                }

                // Reset inventory gameOBject in rat's inventory
                gameObject.GetComponent<RatInventory>().inventoryGameObject = null;

                // Reset ratInventoryGameObject in this component
                ratInventoryGameObject = null;

                // For each tooltip
                for (int i = 0; i < tooltipUI.childCount; i++)
                {
                    // Disable them just to be safe
                    tooltipUI.GetChild(i).gameObject.SetActive(false);
                }
            }
        }
    }
}
