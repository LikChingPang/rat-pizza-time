using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// This script is responsible for the rat's movement
/// </summary>

[RequireComponent(typeof(Rigidbody), typeof(UseItem))]
public class RatMovement : MonoBehaviour
{
    bool isOnRail = false;
    float timer = 0.0f;
    Rigidbody ratRigidbody;
    Vector3 rewindPosition;
    Quaternion rewindRotation;

    public float jumpForce;

    [SerializeField] KeyCode forward = KeyCode.W, backward = KeyCode.S, turnLeft = KeyCode.Q, turnRight = KeyCode.E, moveLeft = KeyCode.A, moveRight = KeyCode.D, jump = KeyCode.Space;
    [SerializeField] float movementSpeed = 6000f, maxMovementSpeed = 5f, turningSpeed = 200f, rewindTime = 2.0f;
    [SerializeField] GroundDetecter groundDetecter;

    void Start()
    {
        // Initialize
        ratRigidbody = transform.GetComponent<Rigidbody>();
        timer = 0.0f;
        rewindPosition = transform.position;
        rewindRotation = transform.rotation;
    }

    void Update()
    {
        // If the timer passed is more than the desired rewind time and the rat is not on the rail and the rat is on the ground
        if (timer >= rewindTime && isOnRail == false && groundDetecter.isOnGround == true)
        {
            // Save new position
            rewindPosition = transform.position;

            // Save new rotation
            rewindRotation = transform.rotation;

            // Reset the timer
            timer = 0.0f;
        }
        // If the rat is on the rail
        else if (isOnRail == true)
        {
            // Rewind position
            transform.position = rewindPosition;

            // Rewind rotation
            transform.rotation = rewindRotation;

            // Reset timer
            timer = 0.0f;

            // Reset isOnRail
            isOnRail = false;
        }
        // If the timer is less than the desired rewind time
        else if (timer < rewindTime)
        {
            // Increament the timer by real time passed
            timer += Time.deltaTime;
        }

        //I f the jump key is pressed
        if (Input.GetKeyDown(jump) && groundDetecter.isOnGround == true)
        {
            // Add upward force
            ratRigidbody.AddForce(transform.up * jumpForce);
        }

        // If the forward key is pressed
        if (Input.GetKey(forward))
        {
            // if the rat's velocity is less than or equal to maxMovementSpeed
            if (transform.GetComponent<Rigidbody>().velocity.magnitude <= maxMovementSpeed)
            {
                // Add forward force
                ratRigidbody.AddForce(movementSpeed * transform.forward * Time.deltaTime);
            }
        }

        // If the backward key is pressed
        if (Input.GetKey(backward))
        {
            // if the rat's velocity is less than or equal to maxMovementSpeed
            if (transform.GetComponent<Rigidbody>().velocity.magnitude <= maxMovementSpeed)
            {
                // Add backward force
                ratRigidbody.AddForce(-movementSpeed * transform.forward * Time.deltaTime);
            }
        }

        // If the moveLeft key is pressed
        if (Input.GetKey(moveLeft))
        {
            // if the rat's velocity is less than or equal to maxMovementSpeed
            if (transform.GetComponent<Rigidbody>().velocity.magnitude <= maxMovementSpeed)
            {
                // Add left force
                ratRigidbody.AddForce(-movementSpeed * transform.right * Time.deltaTime);
            }
        }

        // If the moveRight key is pressed
        if (Input.GetKey(moveRight))
        {
            // if the rat's velocity is less than or equal to maxMovementSpeed
            if (transform.GetComponent<Rigidbody>().velocity.magnitude <= maxMovementSpeed)
            {
                // Add right force
                ratRigidbody.AddForce(movementSpeed * transform.right * Time.deltaTime);
            }
        }

        // If the turnLeft key is pressed
        if (Input.GetKey(turnLeft))
        {
            // Turn left
            transform.Rotate(new Vector3(0, -turningSpeed * Time.deltaTime, 0));
        }

        // If the turnRight key is pressed
        if (Input.GetKey(turnRight))
        {
            // Turn right
            transform.Rotate(new Vector3(0, turningSpeed * Time.deltaTime, 0));
        }
    }

    private void OnCollisionStay(Collision collision)
    {
        // If the rat is on the rail
        if (collision.gameObject.GetComponent<Rail>() != null)
        {
            // Set isOnRail to true
            isOnRail = true;
        }
        else
        {
            // Reset isOnRail
            isOnRail = false;
        }
    }
}
