using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WinTrigger : MonoBehaviour
{
    [SerializeField] GameObject player, stationaryUI, cutSceneCanvas, winImage;

    void OnTriggerEnter(Collider other)
    {
        // If the rat hits the trigger
        if (other.GetComponent<RatInventory>())
        {
            // Play win cutscene
            Debug.Log("Player wins");
            player.SetActive(false);
            stationaryUI.SetActive(false);
            winImage.SetActive(true);
            cutSceneCanvas.SetActive(true);

            // Play win music
        }
    }
}
