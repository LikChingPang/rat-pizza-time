using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// This script is responsible for handling all item usage
/// </summary>

[RequireComponent(typeof(InteractTooltipPopup), typeof(Rigidbody), typeof(RatInventory))]
public class UseItem : MonoBehaviour
{
    public KeyCode useItem = KeyCode.U;

    GameObject target;

    [SerializeField] float boostUpForce = 500;
    [SerializeField] GameObject useItemTooltip;
    [SerializeField] CircuitBoard circuitBoard;
    [SerializeField] Fan fan;
    [SerializeField] Transform backObjects;

    void Update()
    {
        // If the useItem key is pressed and the useItemTooltip is active
        if (Input.GetKeyDown(useItem) && useItemTooltip.activeSelf == true)
        {
            // Get the target gameObject
            target = gameObject.GetComponent<InteractTooltipPopup>().targetGameObject;

            GameObject inventoryGameObject = gameObject.GetComponent<RatInventory>().inventoryGameObject;

            // If there is a target
            if (target != null)
            {
                // If the target is a Mushroom
                if (target.GetComponent<Mushroom>() != null)
                {
                    // Get the target's transform
                    Transform targetTransform = target.transform;

                    // Get the target's collider
                    BoxCollider targetBoxCollider = targetTransform.GetComponent<BoxCollider>();

                    // Snap the rat right on top of the Mushroom
                    transform.position = new Vector3(targetTransform.position.x, targetTransform.position.y + targetBoxCollider.bounds.size.y, targetTransform.position.z);

                    // Boost the rat up with boostUpForce
                    transform.GetComponent<Rigidbody>().AddForce(Vector3.up * boostUpForce);

                    target.GetComponent<AudioSource>().Play();
                }
                // If the target is a wire and the rat is carrying something
                else if (target.GetComponent<Wire>() != null && inventoryGameObject != null)
                {
                    // If the inventory item is a Pepperoni
                    if (inventoryGameObject.GetComponent<Pepperoni>() != null)
                    {
                        // Break the wire
                        target.GetComponent<Wire>().brokenState.SetActive(true);
                        target.GetComponent<Wire>().normalState.SetActive(false);

                        // Set isBroken to true
                        target.GetComponent<Wire>().isBroken = true;

                        // Consume the Pepperoni
                        // Remove the reference in the inventory
                        gameObject.GetComponent<RatInventory>().inventoryGameObject = null;

                        // Remove the reference of the target in InteractTooltipPopup
                        gameObject.GetComponent<InteractTooltipPopup>().targetGameObject = null;

                        // Disable the tooltip
                        useItemTooltip.SetActive(false);

                        // Disable the Pepperoni
                        DisableBackObjects();
                    }
                    // If the inventory item is a Cheese
                    else if (inventoryGameObject.GetComponent<Cheese>() != null)
                    {
                        // If the wire was broken
                        if (target.GetComponent<Wire>().isBroken == true)
                        {
                            // If the wire is not cheesed yet
                            if (target.GetComponent<Cheesed>().cheesed == false)
                            {
                                // Set isBroken to false
                                target.GetComponent<Wire>().isBroken = false;

                                // Consume the Cheese
                                // Remove the reference in the inventory
                                gameObject.GetComponent<RatInventory>().inventoryGameObject = null;

                                // Remove the reference of the target in InteractTooltipPopup
                                gameObject.GetComponent<InteractTooltipPopup>().targetGameObject = null;

                                // Set cheesed to true
                                target.GetComponent<Cheesed>().cheesed = true;

                                // Fix the wire
                                target.GetComponent<Wire>().brokenState.SetActive(false);
                                target.GetComponent<Cheesed>().cheesedBrokenState.SetActive(false);
                                target.GetComponent<Cheesed>().cheesedNormalState.SetActive(true);
                            }
                            // If the wire is cheesed
                            else
                            {
                                // If the wire is not broken yet
                                if (target.GetComponent<Wire>().isBroken == true)
                                {
                                    // Set isBroken to false
                                    target.GetComponent<Wire>().isBroken = false;

                                    // Fix the wire
                                    target.GetComponent<Cheesed>().cheesedBrokenState.SetActive(false);
                                    target.GetComponent<Cheesed>().cheesedNormalState.SetActive(true);
                                }
                                else
                                {
                                    // Set isBroken to true
                                    target.GetComponent<Wire>().isBroken = true;

                                    // Break the wire
                                    target.GetComponent<Cheesed>().cheesedBrokenState.SetActive(true);
                                    target.GetComponent<Cheesed>().cheesedNormalState.SetActive(false);
                                }

                                // GTFO
                                return;
                            }
                        }
                        // If that cheesed wire is not broken yet
                        else
                        {
                            // Rebreak the wire
                            // Set isBroken to true
                            target.GetComponent<Wire>().isBroken = true;

                            // Break the wire
                            target.GetComponent<Cheesed>().cheesedBrokenState.SetActive(true);
                            target.GetComponent<Cheesed>().cheesedNormalState.SetActive(false);

                            // GTFO
                            return;
                        }

                        // Disable the tooltip
                        useItemTooltip.SetActive(false);

                        // Disable the Cheese
                        DisableBackObjects();
                    }

                    // If the fan is not jammed yet
                    if (fan.jammed == false)
                    {
                        // Check if the fan should be jammed
                        fan.CheckStatus();
                    }

                    // If the wire belongs to the circuit board
                    if (target.GetComponent<CirciutBoardWire>() != null)
                    {
                        // If the circuit board is not completed yet
                        if (circuitBoard.isComplete == false)
                        {
                            // Input that wire order
                            circuitBoard.InputOrder(target.transform.parent.GetComponent<LightWire>());
                        }
                    }
                }
                // If the target is a wire that can be cheesed
                else if (target.GetComponent<Wire>() != null && target.GetComponent<Cheesed>() != null)
                {
                    // Fatch the wire component
                    Wire wire = target.GetComponent<Wire>();
                    Cheesed cheesedWire = target.GetComponent<Cheesed>();

                    // If the wire is currently broken
                    if (wire.isBroken == true)
                    {
                        // Fix the wire
                        wire.isBroken = false;

                        // Enable the unbroken state
                        cheesedWire.cheesedNormalState.SetActive(true);

                        // Disable the broken state
                        cheesedWire.cheesedBrokenState.SetActive(false);
                    }
                    // If the wire is not currently broken
                    else
                    {
                        // Fix the wire
                        wire.isBroken = true;

                        // Disable the unbroken state
                        cheesedWire.cheesedNormalState.SetActive(false);

                        // Enable the broken state
                        cheesedWire.cheesedBrokenState.SetActive(true);
                    }

                    // If the fan is not jammed yet
                    if (fan.jammed == false)
                    {
                        // Check if the fan should be jammed
                        fan.CheckStatus();
                    }

                    // If the wire belongs to the circuit board
                    if (target.GetComponent<CirciutBoardWire>() != null)
                    {
                        // If the circuit board is not completed yet
                        if (circuitBoard.isComplete == false)
                        {
                            // Input that wire order
                            circuitBoard.InputOrder(target.transform.parent.GetComponent<LightWire>());
                        }
                    }
                }
            }
        }
    }

    void DisableBackObjects()
    {
        // For each item in the backItems
        for (int i = 0; i < backObjects.childCount; i++)
        {
            // Disable the gameObject
            backObjects.GetChild(i).gameObject.SetActive(false);
        }
    }
}
