using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// This script is responsible for fan blade's behaviour
/// </summary>

[RequireComponent(typeof(Fan))]
public class FanBlades : MonoBehaviour
{
    Vector3 centrePosition = Vector3.zero;
    Quaternion originalRotation;

    [SerializeField] float roateAngle = 10.0f;
    [SerializeField] Transform centreTransform;

    void Start()
    {
        // Initialize
        centrePosition = centreTransform.position;

        // Save the original rotation of the fan blade
        originalRotation = transform.GetChild(2).rotation;
    }

    // For every 0.2 seconds in real time
    void FixedUpdate()
    {
        // If the fan is not jammed yet
        if (gameObject.GetComponent<Fan>().jammed == false)
        {
            // For each blade and the centre piece
            for (int i = 0; i < transform.childCount; i++)
            {
                // Rotate around the centre 
                transform.GetChild(i).transform.RotateAround(centrePosition, Vector3.right, roateAngle);
            }
        }
        // Else
        else
        {
            // For each blade and the centre place
            for (int i = 0; i < transform.childCount; i++)
            {
                // Reset them to the original rotation
                transform.GetChild(i).transform.rotation = originalRotation;
            }
        }
    }
}
