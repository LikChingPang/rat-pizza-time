using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

/// <summary>
/// This script is responsible for detecting if an item is within interact distance.
/// This script is also responsible for asking the tooltipUI to pop up
/// </summary>

[RequireComponent(typeof(ItemPickUp), typeof(UseItem))]
public class InteractTooltipPopup : MonoBehaviour
{
    string interactTooltipString, useItemTooltipString;

    [HideInInspector] public GameObject targetGameObject = null;
    RatInventory ratInventory;
    [SerializeField] float interactableDistance = 4.0f;
    [SerializeField] TMP_Text interactTooltipText, useItemTooltipText;

    void Start()
    {
        // Initialize
        ratInventory = transform.GetComponent<RatInventory>();
        interactTooltipString = transform.GetComponent<ItemPickUp>().interact.ToString();
        useItemTooltipString = transform.GetComponent<UseItem>().useItem.ToString();
    }

    void FixedUpdate()
    {
        Ray mousePosition = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;

        // Send a raycast from the mousePosition
        if (Physics.Raycast(mousePosition, out hit))
        {
            // If the raycast hit a gameObject with Interactable component and the distance between the rat and the gameObject isn't too far
            if (Vector3.Distance(hit.collider.gameObject.transform.position, transform.position) <= interactableDistance)
            {
                // Set the targetGameObject to whatever the raycast is hitting
                targetGameObject = hit.collider.gameObject;

                // If the target is a Wire
                if (targetGameObject.GetComponent<Wire>() != null)
                {
                    // If the target can be cheesed and the wire is currently broken
                    if (targetGameObject.GetComponent<Cheesed>() != null)
                    {
                        // If the wire is already cheesed
                        if (targetGameObject.GetComponent<Cheesed>().cheesed == true)
                        {
                            // Ask the tooltipUI to show the tooltip
                            TooltipUI.Show(useItemTooltipText, useItemTooltipString);
                        }
                    }

                    // If there is currently an inventoryGameObject
                    if (ratInventory.inventoryGameObject != null)
                    {
                        // If the inventory item is currently a Pepperoni and the wire is not broken
                        if (ratInventory.inventoryGameObject.GetComponent<Pepperoni>() != null && targetGameObject.GetComponent<Wire>().isBroken == false)
                        {
                            // Ask the tooltipUI to show the tooltip
                            TooltipUI.Show(useItemTooltipText, useItemTooltipString);
                        }
                        // If the inventory item is currently a Cheese and the wire is broken
                        else if (ratInventory.inventoryGameObject.GetComponent<Cheese>() != null && targetGameObject.GetComponent<Wire>().isBroken == true)
                        {
                            // If the wire can be cheesed
                            if (targetGameObject.GetComponent<Cheesed>() != null)
                            {
                                // Ask the tooltipUI to show the tooltip
                                TooltipUI.Show(useItemTooltipText, useItemTooltipString);
                            }
                        }
                    }
                }

                // If the target has Interactable
                if (hit.collider.gameObject.GetComponent<Interactable>() != null)
                {
                    // If the gameObject is currently interactable
                    if (hit.collider.gameObject.GetComponent<Interactable>().enabled == true)
                    {
                        // If the targetGameObject is interactable
                        if (targetGameObject.GetComponent<Interactable>() != null)
                        {
                            // If the targetGameObject can be interacted at this moment
                            if (targetGameObject.GetComponent<Interactable>().enabled == true)
                            {
                                // Ask the tooltipUI to show the tooltip
                                TooltipUI.Show(interactTooltipText, interactTooltipString);
                            }
                        }
                        // If the targetGameObject is useable
                        if (targetGameObject.GetComponent<Useable>() != null)
                        {
                            // If the targetGameObject can be used at this moment
                            if (targetGameObject.GetComponent<Useable>().enabled == true)
                            {
                                // Ask the tooltipUI to show the tooltip
                                TooltipUI.Show(useItemTooltipText, useItemTooltipString);
                            }
                        }
                    }
                }
            }
            else
            {
                // Set the targetGameObject to null
                targetGameObject = null;

                // Ask the tooltipUI to hide the tooltips
                TooltipUI.Hide();
            }
        }
    }
}
