using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// This script is responsible for the door open trigger,
/// the trigger is determine if the oven door should open
/// </summary>

public class DoorOpenTrigger : MonoBehaviour
{
    [SerializeField] OvenDoor ovenDoor;

    void OnTriggerEnter(Collider other)
    {
        // If the object collided is the Big Smasher
        if (other.GetComponent<BigSmasher>() != null)
        {
            // Close the oven door
            ovenDoor.CloseDoor();
        }
    }

    void OnTriggerExit(Collider other)
    {
        // If the object collided is the Big Smasher
        if (other.GetComponent<BigSmasher>() != null)
        {
            // Close the oven door
            ovenDoor.OpenDoor();
        }
    }
}
