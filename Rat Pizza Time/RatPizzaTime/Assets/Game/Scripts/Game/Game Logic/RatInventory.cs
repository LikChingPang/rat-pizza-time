using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// This script is resposible for the rat's inventory management
/// </summary>

public class RatInventory : MonoBehaviour
{
    [HideInInspector] public GameObject inventoryGameObject;
}
