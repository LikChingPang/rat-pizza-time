using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

/// <summary>
/// This script is responsible for the tooltip text pop up
/// </summary>

public class TooltipUI : MonoBehaviour
{
    public static TooltipUI Instance;

    [SerializeField] Transform toolTipUITransform;
    [SerializeField] RectTransform tooltipUIRectTransform, PopupTooltipCanvasRectTransform;

    void Awake()
    {
        // Initialize
        Instance = this;

        // Hide the tooltip by dafulat
        HideTooltip();
    }

    // For each 0.2 seconds
    void FixedUpdate()
    {
        // Get the new cursor aposition
        Vector2 anchoredPosition = Input.mousePosition / PopupTooltipCanvasRectTransform.localScale.x;

        // Set it to whereever the cursor is
        tooltipUIRectTransform.anchoredPosition = anchoredPosition;
    }

    // Method to set the tooltip text
    void SetText(TMP_Text UIText, string tooltipText)
    {
        // Set the tooltipUIText to the desired text
        UIText.text = tooltipText;
    }

    void ShowTooltip(TMP_Text UIText, string tooltipText)
    {
        // Set the tooltip to the new text
        SetText(UIText, tooltipText);
        // Show the tooltip gameObject
        UIText.gameObject.SetActive(true);
    }

    void HideTooltip()
    {
        // For every tooltip 
        for (int i = 0; i < toolTipUITransform.childCount; i++)
        {
            // Hide the tool tip gameObject
            toolTipUITransform.GetChild(i).gameObject.SetActive(false);
        }
    }

    // Method for other scripts to call the ShowTooltip Method
    public static void Show(TMP_Text UIText, string tooltipText)
    {
        // Show the tooltip
        Instance.ShowTooltip(UIText, tooltipText);
    }

    // Method for other scripts to call the HideTooltip Method
    public static void Hide()
    {
        // Hide the tooltip
        Instance.HideTooltip();
    }
}
