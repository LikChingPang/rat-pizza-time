using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// This script is responsible for picking up an item
/// </summary>

[RequireComponent(typeof(RatInventory), typeof(AudioSource))]
public class ItemPickUp : MonoBehaviour
{
    InteractTooltipPopup interactTooltipPopup;
    RatInventory ratInventory;

    public KeyCode interact = KeyCode.E;

    [SerializeField] CircuitBoard circuitBoard;
    [SerializeField] GameObject backMushroom, backPepperoni, backCheese, backBacon;
    [SerializeField] BigSmasher bigSmasher;
    [SerializeField] Transform tooltipTransform;

    void Start()
    {
        // Initialize
        interactTooltipPopup = gameObject.GetComponent<InteractTooltipPopup>();
        ratInventory = gameObject.GetComponent<RatInventory>();
    }

    void Update()
    {
        // If the tooltip gameObject is currently active
        // aka the cursor is pointing at a gameObject that is interactable
        if (tooltipTransform.GetChild(0).gameObject.activeSelf == true)
        {
            // Get the targeting gameObject
            GameObject target = interactTooltipPopup.targetGameObject;

            // If the interact key is pressed and the rat is not carrying any object
            if (Input.GetKeyDown(interact) && ratInventory.inventoryGameObject == null)
            {
                // If the target is a reset button
                if (target.GetComponent<ResetButton>() != null)
                {
                    // Reset the puzzle
                    circuitBoard.ResetPuzzle();

                    // Play button sound
                    target.GetComponent<AudioSource>().Play();

                    // GTFO
                    return;
                }

                // If the target is the big red button at the end
                if (target.GetComponent<BigRedButton>() != null)
                {
                    // Boost up the smasher
                    bigSmasher.BoostUp();

                    // Play button sound
                    target.GetComponent<AudioSource>().Play();

                    // GTFO
                    return;
                }

                // Safety 
                if (target.GetComponent<Interactable>() != null)
                {
                    // Play the pickup sound
                    gameObject.GetComponent<AudioSource>().Play();

                    // Disable the target gameObject being picked up
                    target.gameObject.SetActive(false);

                    // Save the target gameObject to the inventory
                    ratInventory.inventoryGameObject = target;

                    // If the target gameObject is a Mushroom
                    if (target.GetComponent<Mushroom>() != null)
                    {
                        // Show the Mushroom on the rat
                        backMushroom.SetActive(true);
                    }
                    // If the target gameObject is a Pepperoni
                    else if (target.GetComponent<Pepperoni>() != null)
                    {
                        // Show the Pepperoni on the rat
                        backPepperoni.SetActive(true);
                    }
                    // If the target gameObject is a Cheese
                    else if (target.GetComponent<Cheese>() != null)
                    {
                        // Reset the Kinematic just in case the chaese is on the wall
                        target.GetComponent<Rigidbody>().isKinematic = false;

                        // Show the Cheese on the rat
                        backCheese.SetActive(true);
                    }
                    // If the target gameObject is a Bacon
                    else if (target.GetComponent<Bacon>() != null)
                    {
                        // Show the Cheese on the rat
                        backBacon.SetActive(true);
                    }
                    // Else
                    else
                    {
                        // Give out an error message
                        Debug.Log("There is an error!");
                    }

                    // For each tooltip
                    for (int i = 0; i < tooltipTransform.transform.childCount; i++)
                    {
                        // Disable the tooltip
                        tooltipTransform.transform.GetChild(i).gameObject.SetActive(false);
                    }
                }
            }
        }
    }
}
