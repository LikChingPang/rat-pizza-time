using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    [HideInInspector] public float timer = 0.0f;

    [SerializeField] Color normalLightColor, redLightColor, redderLightColor;
    [SerializeField] float timeLimitInSeconds = 0.0f;
    [SerializeField] Transform backgroundLightsEmptyGameObject;
    [SerializeField] GameObject player, stationaryUI, cutSceneCanvas, loseImage;

    void Start()
    {
        // Initialize
        timer = 0.0f;
    }

    void Update()
    {
        // If the current timer is not at limit yet
        if (timer <= timeLimitInSeconds)
        {
            // Increament the timer
            timer += Time.deltaTime;

            if (timer >= timeLimitInSeconds * 0.67)
            {
                for (int i = 0; i < backgroundLightsEmptyGameObject.childCount; i++)
                {
                    backgroundLightsEmptyGameObject.GetChild(i).GetComponent<BackgroundLights>().SwitchColor(redderLightColor);
                }
            }
            else if (timer >= timeLimitInSeconds * 0.33)
            {
                for (int i = 0; i < backgroundLightsEmptyGameObject.childCount; i++)
                {
                    backgroundLightsEmptyGameObject.GetChild(i).GetComponent<BackgroundLights>().SwitchColor(redLightColor);
                }
            }
        }
        // Else
        else
        {
            // Play lose sound

            // Play lose Cutscene
            Debug.Log("Player loses");
            player.SetActive(false);
            stationaryUI.SetActive(false);
            loseImage.SetActive(true);
            cutSceneCanvas.SetActive(true);
        }
    }
}
