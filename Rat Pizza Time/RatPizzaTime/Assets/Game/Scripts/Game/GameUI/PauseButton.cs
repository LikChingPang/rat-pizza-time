using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// This script is responsible for the behaviour of the pause button aka pause game
/// </summary>

[RequireComponent(typeof(Button))]
public class PauseButton : MonoBehaviour
{
    [SerializeField] GameObject pauseMenu;

    public void PauseGame()
    {
        // Pause the timescale
        Time.timeScale = 0;

        // Hide the Pause Button
        gameObject.SetActive(false);

        // Show the pause menu
        pauseMenu.SetActive(true);
    }
}
