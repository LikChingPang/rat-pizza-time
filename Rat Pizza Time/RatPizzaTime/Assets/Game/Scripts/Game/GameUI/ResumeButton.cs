using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// This script is responsible for the behaviour of the resume button aka resuming the game after paused
/// </summary>

[RequireComponent(typeof(Button))]
public class ResumeButton : MonoBehaviour
{
    [SerializeField] GameObject pauseButton;

    public void ResumeGame()
    {
        // Reset Timescale
        Time.timeScale = 1;

        // Show the pauseButton
        pauseButton.SetActive(true);

        // Hide the pause menu
        transform.parent.gameObject.SetActive(false);
    }
}
