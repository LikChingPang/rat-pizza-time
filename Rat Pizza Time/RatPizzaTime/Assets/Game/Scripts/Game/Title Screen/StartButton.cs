using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

/// <summary>
/// This script is responsible for the behaviour of the start button aka start game
/// </summary>

[RequireComponent(typeof(Button))]
public class StartButton : MonoBehaviour
{
    [SerializeField] string gameSceneName;

    public void StartGame()
    {
        SceneManager.LoadScene(gameSceneName);
    }
}
